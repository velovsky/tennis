var manager = 
{
	vars: 
	{
		1: "Player 1",
		2: "Player 2",
		players_score:
		{
			1: {score:0, advantage:false, score_text:"Love"},
			2: {score:0, advantage:false, score_text:"Love"},
		},
		rules:
		{
			0: "Love",
			1: "Fifteen",
			2: "Thirty",
			3: "Fourty",
			4: "Win for",
			advantage: "Advantage"
		}
	},
	main: function()
	{		
		//ref output(s) label
		manager.vars.round_label = $("#round")[0];
		manager.vars.round_winner_label = $("#round_winner")[0];
		manager.vars.result_label = $("#result")[0];
		manager.vars.output_label = $("#output_field")[0];
		
		//get round winner
		manager.vars.round = parseInt(manager.vars.round_label.innerHTML);
		manager.vars.round_winner = manager.round_winner();
		//get loser of round
		if(manager.vars.round_winner === 1)
			manager.vars.round_loser = 2;
		else
			manager.vars.round_loser = 1;
		
		//compute score text
		manager.compute_score();
		
		//set output
		manager.output();
	},
	round_winner: function()
	{
		//random: [0,1[; *2 [0,2[; +1 [1,3[; floor round down: [1,2[ = 1, [2,3[ = 2
		var player = Math.floor((Math.random() * 2) + 1);
		return player;
	},
	disable_button: function() //when match ends
	{
		$("#submit_button")[0].disabled = true;
		$("#submit_button")[0].value = "Match Ended!";
	},
	compute_score: function()
	{
		
		//result text
		var score_text = "";
		
		//check current score
		var player1 = manager.vars.players_score[1];
		var player2 = manager.vars.players_score[2];
		var round_winner = manager.vars.round_winner;
		var round_loser = manager.vars.round_loser;
		
		//rules
		var rules = manager.vars.rules;
		
		//Scoring rules
		if(player1.score >= 3 && player2.score >= 3)
		{
			if(manager.vars.players_score[round_winner].advantage) //if it has advantage
			{
				manager.vars.players_score[round_winner].score++;
				manager.vars.players_score[round_winner].score_text = rules[manager.vars.players_score[round_winner].score] + " " +
				manager.vars[round_winner];
				manager.disable_button();
			}
			else
			{
				if(!manager.vars.players_score[round_loser].advantage) //if round loser doesnt have advantage
				{
					//set advantage to winning player
					manager.vars.players_score[round_winner].advantage = true;
					manager.vars.players_score[round_winner].score_text = rules.advantage + " " + manager.vars[round_winner];
				}
				//remove advantage to losing player
				manager.vars.players_score[round_loser].advantage = false;
				manager.vars.players_score[round_loser].score_text = "";
			}
			//set score text
			if(manager.vars.players_score[round_winner].advantage === false 
			&& manager.vars.players_score[round_loser].advantage === false) //deuce
				score_text = "Deuce";
			else
				score_text = manager.vars.players_score[round_winner].score_text;
		}
		else
		{
			manager.vars.players_score[round_winner].score++;
			manager.vars.players_score[round_winner].score_text = rules[manager.vars.players_score[round_winner].score];
			
			//set score text
			if(manager.vars.players_score[1].score === manager.vars.players_score[2].score) //tied
			{
				if(manager.vars.players_score[1].score >= 3)
					score_text = "Deuce";
				else
					score_text = manager.vars.players_score[1].score_text + "-All";
			}
			else if(manager.vars.players_score[round_winner].score === 4) //Won game
			{
				score_text = manager.vars.players_score[round_winner].score_text + " " + manager.vars[round_winner];
				manager.disable_button();
			}
			else
				score_text = manager.vars.players_score[1].score_text + "-" + manager.vars.players_score[2].score_text;
		}
		
		manager.vars.result = player1.score + " - " + player2.score;
		manager.vars.score_text = score_text;
		
	},
	output: function(array)
	{	
		//set round output
		var round_winner_text = "(" + manager.vars[manager.vars.round_winner] + " won this round!)";
		manager.vars.round_label.innerHTML = manager.vars.round + 1;
		manager.vars.round_winner_label.innerHTML = round_winner_text;
		
		//set result output
		manager.vars.result_label.innerHTML = manager.vars.result;
		manager.vars.output_label.innerHTML = manager.vars.score_text;
	}
}